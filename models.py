import webapp2
from google.appengine.ext import ndb
import datetime
from httplib import HTTPException
import socket

class EveAccount(ndb.Model):
    user = ndb.UserProperty()
    key_id = ndb.IntegerProperty()
    key_secret = ndb.StringProperty()


class Item(ndb.Model):
    item_id = ndb.IntegerProperty()
    name = ndb.StringProperty()

    watch = ndb.BooleanProperty()


class ImplantObject(ndb.Model):
    type_id = ndb.IntegerProperty()

    memory_mod = ndb.IntegerProperty()
    willpower_mod = ndb.IntegerProperty()
    intelligence_mod = ndb.IntegerProperty()
    perception_mod = ndb.IntegerProperty()
    charisma_mod = ndb.IntegerProperty()


class Skill(ndb.Model):
    type_id = ndb.IntegerProperty()

    primary = ndb.StringProperty()
    secondary = ndb.StringProperty()
    mp = ndb.IntegerProperty()


class LoadOp(ndb.Model):
    items_left = ndb.IntegerProperty()
    next = ndb.JsonProperty()

class DBCache(ndb.Model):
    char_id = ndb.IntegerProperty()
    cache_key = ndb.StringProperty()
    date = ndb.DateTimeProperty()
    data = ndb.JsonProperty()

    @classmethod
    def get_or_cache(cls, char_id, get_function, cache_key):
        cacheObj = cls.query(cls.char_id == char_id, cls.cache_key == cache_key).get()
        now = datetime.datetime.now()

        age = None
        if cacheObj:
            age = (now - cacheObj.date).total_seconds()
        
        if cacheObj and age < 60 * 60:
            return cacheObj.data

        try:
            result = get_function()
        except (HTTPException,socket.error) as e:
            if not cacheObj or age > 60 * 60 * 24:
                raise e

            return cacheObj.data

        
        if not cacheObj:
            cacheObj = DBCache()
            cacheObj.char_id = char_id
            cacheObj.cache_key = cache_key

        cacheObj.date = now
        cacheObj.data = result

        cacheObj.put()

        return result

class MarketStats(ndb.Model):
    type_id = ndb.IntegerProperty()
    date = ndb.DateTimeProperty()
    operation = ndb.StringProperty()

    volume = ndb.IntegerProperty()
    avg_price = ndb.FloatProperty()
    max_price = ndb.FloatProperty()
    min_price = ndb.FloatProperty()
    std_dev = ndb.FloatProperty()
    median = ndb.FloatProperty()
    percentile = ndb.FloatProperty()

class Requirement(ndb.Model):
    type_id = ndb.IntegerProperty()
    amount = ndb.IntegerProperty()

class Blueprint(ndb.Model):
    type_id = ndb.IntegerProperty()
    result_type_id = ndb.IntegerProperty()
    result_amount = ndb.IntegerProperty()
    
    requirements = ndb.StructuredProperty(Requirement, repeated=True)
    


var app = angular.module('eve', []);

var EVENT_ACCOUNT_SET = "AccountSet";
var EVENT_CHARACTER_SET = "CharacterSet";
var EVENT_UPDATE_TICK = "UpdateTick";

// App wide timer, makes sure all the timers on the page get updated at the same time
app.run(function($rootScope, $interval){
    /*setInterval(
        function(){
            $rootScope.$apply(function(){
                $rootScope.$broadcast(EVENT_UPDATE_TICK, Math.floor(Date.now() / 1000));
            })
        }, 
        1000
    );*/

    $interval(function(){ $rootScope.$broadcast(EVENT_UPDATE_TICK, Math.floor(Date.now() / 1000)); }, 1000)
});

// Format timespan from seconds
app.filter("timespan_format", function(){
    return function(span){
        var sec_num = parseInt(span, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
    }
});

// Skills controller
app.controller('skills', function($rootScope, $scope, $http){
    $scope.account = null;
    $scope.char_id = null;
    $scope.time = Math.floor(Date.now() / 1000);

    $scope.skills = [];

    $rootScope.$on(EVENT_ACCOUNT_SET, function(args, account){
        $scope.account = account;
    });

    $rootScope.$on(EVENT_CHARACTER_SET, function(args, char_id){
        $scope.char_id = char_id;
    });

    $rootScope.$on(EVENT_UPDATE_TICK, function(args, time){                
        $scope.time = time;
    });


    $scope.$watch("account + char_id",function(){
        if($scope.account == null || $scope.char_id == null)  return;

        $scope.skills = [];

        $http.get("/skills/" + $scope.account + "/" + $scope.char_id)
            .success(function(skills){
                $scope.skills = skills.q;
            });
    });
});

function togglable_controller(controller_name, url, cb_setter, cb_getter){

    // Skills controller
    app.controller(controller_name, function($rootScope, $scope, $http){
        $scope.account = null;
        $scope.char_id = null;
        $scope.loaded = {"account":"","char_id":""};

        $scope.open = false;
        $scope.time = Math.floor(Date.now() / 1000);

        cb_setter($scope, null);

        $rootScope.$on(EVENT_ACCOUNT_SET, function(args, account){
            $scope.account = account;
        });

        $rootScope.$on(EVENT_CHARACTER_SET, function(args, char_id){
            $scope.char_id = char_id;
        });

        $rootScope.$on(EVENT_UPDATE_TICK, function(args, time){        
            $scope.time = time;
        });


        $scope.$watch("account + char_id + open",function(){
            if($scope.account == null || $scope.char_id == null)  return;

            if($scope.loaded.account != $scope.account || $scope.loaded.char_id != $scope.char_id)
                cb_setter($scope, null);

            if($scope.open && cb_getter($scope) === null){
                $http.get(url + $scope.account + "/" + $scope.char_id)
                    .success(function(data){
                        cb_setter($scope, data);
                        $scope.loaded.account = $scope.account;
                        $scope.loaded.char_id = $scope.char_id;
                    });
            }
        });
    });
}

togglable_controller(
    "skill_tree", 
    "/skill_tree/", 
    function($scope, data){
        $scope.skill_tree = data;
    },
    function($scope){
        return $scope.skill_tree;
    }
);

togglable_controller(
    "bp_jobs", 
    "/bp_jobs/", 
    function($scope, data){
        $scope.bp_jobs = data;
    },
    function($scope){
        return $scope.bp_jobs;
    }
);



// Characters controller
app.controller('characters', function($rootScope, $scope, $http){
    $scope.account = null;
    $scope.chars = [];

    $rootScope.$on(EVENT_ACCOUNT_SET, function(args, account){
        $scope.account = account;
    });

    $scope.$watch("account",function(){
        if($scope.account == null) return;

        $scope.chars = [];

        $http.get("/characters/" + $scope.account)
            .success(function(chars){
                $scope.chars = chars;
            });
    });

    $scope.select_char = function(character_id){
        $rootScope.$broadcast(EVENT_CHARACTER_SET, character_id);
    };
});

// Accounts controller
app.controller('accounts', function($rootScope, $scope, $http, $timeout){
    $scope.key_id = "";
    $scope.key_secret = "";
    $scope.available = true;
    $scope.accounts = [];


    $http.get("/accounts").success(function(accounts){
        $scope.available = true;
        $scope.accounts = accounts;
    });


    // Remove error after 3 seconds
    $scope.err_msg = null;
    $scope.error_timer = null;
    $scope.$watch("err_msg", function(){
        if($scope.error_time != null){
            $timeout.cancel($scope.error_timer);
            $scope.error_time = null;
        }

        if($scope.err_msg){
            $scope.error_timer = $timeout(function(){
                $scope.err_msg = null;
            }, 3000);
        }
    });

    $scope.send = function(){
        $scope.err_msg = null;

        if($scope.key_id == "" || isNaN($scope.key_id)){
            $scope.err_msg = "Key id not a number";
            return;
        }

        $scope.available = false;

        $http.post(
            "/accounts",
            {"key_id":parseInt($scope.key_id),"key_secret":$scope.key_secret}
        )
            .success(function(){

                var key_id = parseInt($scope.key_id);
                
                if($scope.accounts.indexOf(key_id) == -1)
                    $scope.accounts.push(key_id);

                $scope.key_id = "";
                $scope.key_secret = "";
                $scope.available = true;
            })
            .error(function(error){
                $scope.err_msg = error.msg;

                $scope.key_id = "";
                $scope.key_secret = "";
                $scope.available = true;
            })
    };

    $scope.delete_account = function(account){
        $http.delete("/account/" + account)
            .success(function(){
                var index = $scope.accounts.indexOf(account);
                if(index > -1) $scope.accounts.splice(index, 1);
            });
    };

    $scope.select_account = function(account){
        $rootScope.$broadcast(EVENT_ACCOUNT_SET, account);
    };
});



from os import path,unlink
import sqlite3
import yaml

if path.exists("blueprints.db"):
    unlink("blueprints.db")

with sqlite3.connect("blueprints.db") as conn:
    cursor = conn.cursor()

    cursor.execute("CREATE TABLE blueprints(typeID INT PRIMARY KEY, blueprintTypeID INT, maxProductionLimit INT)")

    # Activities and indexes

    cursor.execute("CREATE TABLE blueprint_activity(bpTypeID INT, activity TEXT, time INT, PRIMARY KEY (bpTypeID, activity))")

    cursor.execute("CREATE INDEX blueprint_activity_idx ON blueprint_activity(bpTypeID)")

    # Activity products and indexes

    cursor.execute("CREATE TABLE blueprint_activity_product(bpTypeID INT, activity TEXT, productTypeId INT, quantity INT, probability REAL, PRIMARY KEY (bpTypeID, activity, productTypeId))")

    cursor.execute("CREATE INDEX blueprint_activity_product_idx ON blueprint_activity_product(bpTypeID, activity)")

    # Activity materials and indexes

    cursor.execute("CREATE TABLE blueprint_activity_material(bpTypeID INT, activity TEXT, materialTypeId INT, quantity INT, PRIMARY KEY (bpTypeID, activity, materialTypeId))")

    cursor.execute("CREATE INDEX blueprint_activity_material_idx ON blueprint_activity_material(bpTypeID, activity)")

    # Activity skills and indexes

    cursor.execute("CREATE TABLE blueprint_activity_skill(bpTypeID INT, activity TEXT, skillTypeId INT, level INT, PRIMARY KEY (bpTypeID, activity, skillTypeId))")

    cursor.execute("CREATE INDEX blueprint_activity_skill_idx ON blueprint_activity_skill(bpTypeID, activity)")

    with open("blueprints.yaml") as fh:
        blueprints = yaml.load(fh)

        for bp_id, bp_data in blueprints.iteritems():
            cursor.execute("INSERT INTO blueprints(typeID, blueprintTypeID, maxProductionLimit) VALUES (?,?,?)", (bp_id, bp_data["blueprintTypeID"], bp_data["maxProductionLimit"]))

            activity_insert = []
            material_insert = []
            product_insert = []
            skill_insert = []

            for activity_name, activity_data in bp_data["activities"].iteritems():
                activity_insert.append((bp_id, activity_name, activity_data["time"]))

                if "materials" in activity_data:
                    for material_data in activity_data["materials"]:
                        material_insert.append((bp_id, activity_name, material_data["typeID"], material_data["quantity"]))
                
                if "products" in activity_data:
                    for product_data in activity_data["products"]:
                        product_insert.append((bp_id, activity_name, product_data["typeID"], product_data["quantity"], product_data["probability"] if "probability" in product_data else 1.0))

                if "skills" in activity_data:
                    for skill_data in activity_data["skills"]:
                        skill_insert.append((bp_id, activity_name, skill_data["typeID"], skill_data["level"]))

            #print(activity_insert, material_insert, product_insert, skill_insert)
            if activity_insert:
                cursor.executemany("INSERT INTO blueprint_activity(bpTypeID, activity, time) VALUES (?,?,?)", activity_insert)

            if material_insert:
                cursor.executemany("INSERT INTO blueprint_activity_material(bpTypeID, activity, materialTypeId, quantity) VALUES (?,?,?,?)", material_insert)

            if product_insert:
                cursor.executemany("INSERT INTO blueprint_activity_product(bpTypeID, activity, productTypeId, quantity, probability) VALUES (?,?,?,?,?)", product_insert)

            if skill_insert:
                try:
                    cursor.executemany("INSERT INTO blueprint_activity_skill(bpTypeID, activity, skillTypeId, level) VALUES (?,?,?,?)", skill_insert)
                except sqlite3.IntegrityError:
                    print("Integrity error on skills:" + str(skill_insert) + " on blueprint id:" + str(bp_id))

    conn.commit()

    cursor = conn.cursor()
    cursor.execute("VACUUM")

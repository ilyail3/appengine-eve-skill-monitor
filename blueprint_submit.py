import sqlite3
import json
import sys
import urllib

if len(sys.argv) != 2:
    print("Expecting type id to submit")

type_id = int(sys.argv[1])

with sqlite3.connect("blueprints.db") as conn:
    cursor = conn.cursor()


    result_obj = {"type_id":type_id, "material":[]}


    cursor.execute("SELECT productTypeId, quantity FROM blueprint_activity_product WHERE bpTypeID = ? AND activity = 'manufacturing'", (type_id,))
    data = cursor.fetchone()

    #bp.result_type_id = data[0]
    result_obj["result_type_id"] = data[0]
    result_obj["result_amount"] = data[1]

    cursor.execute("SELECT materialTypeId, quantity FROM blueprint_activity_material WHERE bpTypeID = ? AND activity = 'manufacturing'", (type_id,))

    requirements = result_obj["material"]
    data = cursor.fetchone()
    while data:
        requirements.append({"type_id":data[0], "amount":data[1]})
        

        data = cursor.fetchone()                                           

    fh = urllib.urlopen("http://modern-dream-91412.appspot.com/copy_bp", json.dumps(result_obj))
    print(fh.read())


import jinja2
import webapp2
import os
from google.appengine.api import users
from models import EveAccount,DBCache,MarketStats,Blueprint,\
    Requirement,Item,LoadOp,ImplantObject,Skill
import json
import evelink.eve as evelink_eve
import evelink.api as evelink_api
import evelink.char as evelink_char
import evelink.account as evelink_account
import re
from urllib import urlopen
from lxml import etree
import datetime
import yaml
from google.appengine.api import taskqueue
from google.appengine.api import memcache
from google.appengine.ext import ndb
import logging
from google.appengine.datastore.datastore_query import Cursor

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + "/templates"),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True,
    variable_start_string='((', 
    variable_end_string='))'
)


class InvalidData(Exception):
    def __init__(self, message):
        self.message = message


def get_item_name(item_id):
    cache_key = "item_%d" % item_id

    item_name = memcache.get(cache_key)

    if item_name:
        return item_name
    else:
        item = Item.query(Item.item_id == item_id).get()
        if not item:
            return "Unkown(%d)" % item_id
    
        memcache.set(key=cache_key, value=item.name)
        return item.name
    

class MainPage(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        data = {"user":user}

        if user:
            data["logout_url"] = users.create_logout_url('/')
        else:
            data["login_url"] = users.create_login_url('/')

        #self.response.out.write("<html><body>%s</body></html>" % greeting)

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(data))

class Accounts(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect(users.create_login_url('/'))

        accounts = EveAccount.query(EveAccount.user == user).fetch(20)

        json.dump([account.key_id for account in accounts], self.response)

    def post(self):
        user = users.get_current_user()

        if not user:
            self.redirect(users.create_login_url('/'))

        post_params = json.load(self.request.body_file)

        try:
            if not post_params["key_id"]:
                raise InvalidData("key_id is missing")

            if not post_params["key_secret"]:
                raise InvalidData("key_secret is missing")

            try:
                key_id = int(post_params["key_id"])
            except ValueError:
                raise InvalidData("key_id must be an integer")

            if key_id <= 0:
                raise InvalidData("Key id must be positive")

            secret = post_params["key_secret"]

            if len(secret) != 64:
                raise InvalidData("Secret is 64 chars long")
        except InvalidData as ex:
            self.response.set_status(400)
            self.response.headers['Content-Type'] = 'application/json'
            self.response.write(json.dumps({"type": "error", "msg": ex.message}))
            return

        account = EveAccount.query(
            EveAccount.user == user,
            EveAccount.key_id == key_id
        ).get()

        if not account:        
            account = EveAccount(
                user=user,
                key_id=key_id,
                key_secret=secret
            )

        account.put()

        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps({"type": "ok"}))


class Account(webapp2.RequestHandler):
    def delete(self, key_id):
        user = users.get_current_user()

        if not user:
            self.redirect(users.create_login_url('/'))

        key_id = int(key_id)

        account = EveAccount.query(EveAccount.user == user, EveAccount.key_id == key_id).get()
        
        account.key.delete()

        self.response.write("ok")


class Characters(webapp2.RequestHandler):
    def get(self, key_id):
        user = users.get_current_user()

        if not user:
            self.redirect(users.create_login_url('/'))

        key_id = int(key_id)
        
        def read_chars():
            account = EveAccount.query(EveAccount.user==user, EveAccount.key_id==key_id).get()

            api = evelink_api.API(api_key=(str(account.key_id),account.key_secret))

            account_api = evelink_account.Account(api)

            #result_set,ts,expires = account_api.characters()
            result_set,_,_ = account_api.characters()

            return result_set

        result_set = DBCache.get_or_cache(key_id, read_chars, "chars")

        result = []

        for account_id, details in result_set.iteritems():
            result.append({
                "id": account_id,
                "name": details["name"]
            })

        json.dump(result, self.response)


class Skills(webapp2.RequestHandler):
    def get(self, key_id, char_id):
        user = users.get_current_user()

        if not user:
            self.redirect(users.create_login_url('/'))

        key_id = int(key_id)

        def read_skills():
            account = EveAccount.query(EveAccount.user==user, EveAccount.key_id==key_id).get()

            api = evelink_api.API(api_key=(str(account.key_id),account.key_secret))

            char = evelink_char.Char(char_id, api)

            return char.skill_queue().result

        result = []

        sheet = SkillTree.read_char_sheet(user, key_id, char_id)

        attrs = SkillTree.read_attr(sheet)

        progress = {}

        for skill in sheet["skills"]:
            progress[skill["id"]] = skill["skillpoints"]

        skills = DBCache.get_or_cache(int(char_id), read_skills, "skills")

        for skill in skills:

            sps = {
                1: 250,
                2: 1415,
                3: 8000,
                4: 45255,
                5: 256000
            }

            type_id = skill["type_id"]

            skill_data = {
                "type_id": type_id,
                "skill": get_item_name(type_id),
                "level": skill["level"],
                "end": skill["end_ts"]
            }

            skill_type = Skill.query(
                Skill.type_id == type_id
            ).get()

            if skill_type:
                # skill_mp = skill["end_sp"] / sps[skill["level"]]
                skill_mp = skill_type.mp

                start = 0
                if skill["level"] > 1:
                    start = sps[skill["level"] - 1] * skill_mp

                trained_sp = 0

                if type_id in progress and progress[type_id] > start:
                    trained_sp = progress[type_id] - start

                skill_data.update({
                    "primary": skill_type.primary,
                    "secondary": skill_type.secondary,
                    "mp": skill_mp,
                    "trained": trained_sp,
                    "total": skill["end_sp"] - start
                })

            result.append(skill_data)

        json.dump({"q": result, "attrs": attrs}, self.response)


class SkillTree(webapp2.RequestHandler):

    @classmethod
    def read_attr(cls, char_sheet):
        res = {}

        for name, value in char_sheet["attributes"].items():
            res[name] = value["base"]

        for implant_id in char_sheet["implants"].keys():
            implant = ImplantObject.query(
                ImplantObject.type_id == int(implant_id)
            ).get()

            if implant:
                res["memory"] += implant.memory_mod
                res["willpower"] += implant.willpower_mod
                res["intelligence"] += implant.intelligence_mod
                res["perception"] += implant.perception_mod
                res["charisma"] += implant.charisma_mod

        return res

    @classmethod
    def read_char_sheet(cls, user, key_id, char_id):
        def read_char_sheet():

            account = EveAccount.query(EveAccount.user == user, EveAccount.key_id == key_id).get()

            api = evelink_api.API(api_key=(str(account.key_id),account.key_secret))

            char = evelink_char.Char(char_id, api)

            return char.character_sheet().result

        return DBCache.get_or_cache(int(char_id), read_char_sheet, "char_sheet")

    @classmethod
    def read_skill_tree(cls, user, key_id, char_id):
        return cls.read_char_sheet(user, key_id, char_id)["skills"]

    def get(self, key_id, char_id):
        user = users.get_current_user()

        if not user:
            self.redirect(users.create_login_url('/'))

        key_id = int(key_id)

        skill_tree = SkillTree.read_skill_tree(user, key_id, char_id)

        result = []

        for skill in skill_tree:
            result.append({
                "name": get_item_name(skill["id"]),
                "level": skill["level"],
                "sp": skill["skillpoints"]
            })            

        json.dump(result, self.response)       

class BPJobs(webapp2.RequestHandler):
    def get(self, key_id, char_id):
        user = users.get_current_user()

        if not user:
            self.redirect(users.create_login_url('/'))

        key_id = int(key_id)

        def read_bp_jobs():

            account = EveAccount.query(EveAccount.user==user, EveAccount.key_id==key_id).get()

            api = evelink_api.API(api_key=(str(account.key_id),account.key_secret))

            char = evelink_char.Char(char_id, api)

            return char.industry_jobs().result

        jobs = DBCache.get_or_cache(int(char_id), read_bp_jobs, "bp_jobs")
        result = []

        for job_id, job in jobs.iteritems():
            activity = ""

            if job["activity_id"] == 1:
                activity = "Manufacturing"
            elif job["activity_id"] == 2:
                activity = "Researching Technology"
            elif job["activity_id"] == 3:
                activity = "Researching Time Productivity"
            elif job["activity_id"] == 4:
                activity = "Researching Material Productivity"
            elif job["activity_id"] == 5:
                activity = "Copying"
            elif job["activity_id"] == 6:
                activity = "Duplicating"
            elif job["activity_id"] == 7:
                activity = "Reverse Engineering"
            elif job["activity_id"] == 8:
                activity = "Invention"
            else:
                activity = "unknown(%d)" % job["activity_id"] 

            result.append({
                "end_ts":job["end_ts"],
                "bp":get_item_name(job["blueprint"]["type"]["id"]).replace(" Blueprint",""),
                "activity":activity,
                "completed":job["completed"],
                "runs":job["runs"]
            })

        json.dump(result, self.response) 

class PriceCapture(webapp2.RequestHandler):
    def get(self):
        track = []

        for item in Item.query(Item.watch == True).fetch(30):
            track.append(item.item_id)

        #TRACK = [
        #    34, # Tritanium
        #    35, # Pyerite
        #    36, # Mexallon
        #    37, # Isogen
        #    38, # Nocxium
        #    39, # Zydrine
        #    40, # Megacyte
        #    17482
        #]
        assert len(track) > 0, "No watched items"

        qparams = "&".join(["typeid=%d" % t for t in track])

        fh = None
        try:
            fh = urlopen("http://api.eve-central.com/api/marketstat?" + qparams)
            data = fh.read()
        finally :
            if fh:
                fh.close()
        
        data = etree.fromstring(data)
        for xml_type in data[0]:
            type_id = int(xml_type.get("id"))
            for op in xml_type:
                stats = MarketStats()
                #operation_name = op.tag
                stats.type_id = type_id
                stats.date = datetime.datetime.now()
                stats.operation = op.tag
                
                stats.volume = int(op.find("volume").text)
                stats.avg_price = float(op.find("avg").text)
                stats.max_price = float(op.find("max").text)
                stats.min_price = float(op.find("min").text)
                stats.std_dev = float(op.find("stddev").text)
                stats.median = float(op.find("median").text)
                stats.percentile = float(op.find("percentile").text)

                stats.put()
                
        self.response.write("ok")

    def post(self):
        self.get()

def watch_item(item_id):
    taskqueue.add(url='/watch/%d/true' % item_id,transactional=False)

class CopyBP(webapp2.RequestHandler):       
    def post(self):
        bp_data = json.loads(self.request.body)

        bp = Blueprint.query(Blueprint.type_id == bp_data["type_id"]).get()

        if not bp:
            bp = Blueprint()
            bp.type_id = bp_data["type_id"]

        bp.result_type_id = bp_data["result_type_id"]

        # Make sure result is watched
        watch_item(bp.result_type_id)

        bp.result_amount = bp_data["result_amount"]

        bp.requirements = []
        for mat in bp_data["material"]:

            req_obj = Requirement()
            req_obj.type_id = mat["type_id"]
            req_obj.amount = mat["amount"]

            watch_item(req_obj.type_id)

            bp.requirements.append(req_obj)                                     

        bp.put()

        self.response.write("ok")


class ProductionProfitMax(webapp2.RequestHandler):
    def format_isk(self, num):
        return format(num,",.2f") + " ISK"

    def get_req_cost(self, req):
        return req.min_price

    def get_sell_cost(self, req):
        return req.max_price

    def get(self, type_id, me):
        me = float(int(me))

        if(me < 0 or me > 10):
            assert False, "me should be from 0 to 10"

        bp = Blueprint.query(Blueprint.type_id == int(type_id)).get()

        sell_price = MarketStats.query(MarketStats.type_id == bp.result_type_id, MarketStats.operation == "buy").order(-MarketStats.date).get()
        sell_price = self.get_sell_cost(sell_price) * bp.result_amount

        self.response.headers['Content-Type'] = 'text/plain'

        self.response.write("Sell price:\n")
        self.response.write("==" * 20 + "\n")
        self.response.write("%s x%d = %s\n\n" % (get_item_name(bp.result_type_id), bp.result_amount, self.format_isk(sell_price)))
        

        total_buy_price = 0.0

        self.response.write("Buy price:\n")
        self.response.write("==" * 20 + "\n")

        for req in bp.requirements:
            required = int(req.amount - (float(req.amount) * me * 0.01))
            buy_price = MarketStats.query(MarketStats.type_id == req.type_id, MarketStats.operation == "sell").order(-MarketStats.date).get()
            unit_price = self.get_req_cost(buy_price)

            buy_price = unit_price * required

            self.response.write("%s x%d = (%s * %d) = %s\n" % (get_item_name(req.type_id), required, self.format_isk(unit_price), required, self.format_isk(buy_price)))

            total_buy_price += buy_price

        
        self.response.write("\nPotential profit:%s - %s = %s\n" % (
            self.format_isk(sell_price), 
            self.format_isk(total_buy_price), 
            self.format_isk(sell_price-total_buy_price)
        ))

class ProductionProfitAvg(ProductionProfitMax):
    def get_req_cost(self, req):
        return req.avg_price

    def get_sell_cost(self, req):
        return req.avg_price

@ndb.transactional
def section_done(op_id):
    op = LoadOp.get_by_id(int(op_id))

    if not op:
        logging.warn("Load operation:%s done, but record missing" % op_id)
        return None
    
    op.items_left -= 1
    op.put()

    logging.info("Load operation:%s has %d more partitions to process" % (op_id, op.items_left))

    if op.items_left == 0:
        op.key.delete()
        return op.next
    else:
        return None

class LoadItems(webapp2.RequestHandler):
    def run(self, mod, val):
        mod = int(mod)
        val = int(val)

        with open(os.path.dirname(__file__) + "/typeid.txt","r") as fh:
            for l in fh.readlines():
                l = l.strip()

                if not l:
                    continue

                sp_index = l.index(" ")
                item_id = int(l[0:sp_index])
                item_name = l[sp_index:].strip()

                if (item_id % mod) == val:                
                    item = Item.query(Item.item_id == item_id).get()
                    if not item:
                        item = Item()
                        item.item_id = item_id
                        item.watch = False

                    if not item.name == item_name:
                        item.name = item_name
                        item.put()

        op = self.request.get("op")
        

        if op:
            next_job = section_done(op)

            if next_job:
                for nj in next_job:
                    kp = {"url":nj["url"], "transactional":False}

                    if "params" in nj:
                        kp["params"] = nj["params"]

                    logging.info("Load operation complete adding task:" + json.dumps(nj))
                    
                    taskqueue.add(**kp)

        self.response.write("ok")

    def get(self, mod, val):
        self.run(mod,val)

    def post(self, mod, val):
        self.run(mod,val)
        

class LoadAllItems(webapp2.RequestHandler):
    def post(self):
        mod = 100

        params = {}
        
        next = self.request.get("next")

        if next:
            op = LoadOp()
            op.items_left = mod
            op.next = json.loads(next)
            op.put()

            params["op"] = str(op.key.id())

        
        for i in range(0,mod):
            taskqueue.add(url='/load_items/%d/%d' % (mod,i), transactional=False, params=params)

        

        self.response.write("ok")

class Watch(webapp2.RequestHandler):
    def get(self, item_id, watch):
        
        item_id = int(item_id)

        item = Item.query(Item.item_id == item_id).get()

        if watch == "true":
            item.watch = True
        else:
            item.watch = False

        item.put()

        if item.watch:
            self.response.write("Item %d(%s) is watching" % (item_id, item.name))
        else:
            self.response.write("Item %d(%s) is not watching" % (item_id, item.name))
    def post(self, item_id, watch):
        self.get(item_id, watch)


class Setup(webapp2.RequestHandler):
    def post(self):
        next_ops = []

        watch_list = range(34,41) # materials
        
        for m in watch_list:
            next_ops.append({"url":'/watch/%d/true' % m})

        #bp_list = [17483] # Copy strip miner I blueprint
        
        #for bp in bp_list:
        #    next_ops.append({"url":'/copy_bp/%d' % bp})

        taskqueue.add(url='/load_items', params = {
            "next":json.dumps(next_ops)
        },transactional=False)
        
        self.response.write("Setup command sent")

class Export(webapp2.RequestHandler):
    def get(self, type_id):
        type_id = int(type_id)

        kwargs = {}
        if self.request.get("cursor"):
            kwargs["start_cursor"] = Cursor(urlsafe=self.request.get('cursor'))

        items, next_curs, more = MarketStats.query(MarketStats.type_id == type_id)\
            .order(MarketStats.date).fetch_page(1000, **kwargs)

        import csv
        import StringIO

        data = StringIO.StringIO()

        writer = csv.writer(data)

        writer.writerow([
            "date",
            "operation",
            "volume",
            "avg_price",
            "max_price",
            "min_price",
            "std_dev",
            "median",
            "percentile"
        ])

        for record in items:
            writer.writerow([
                record.date.isoformat(),
                record.operation,
                str(record.volume),
                str(record.avg_price),
                str(record.max_price),
                str(record.min_price),
                str(record.std_dev),
                str(record.median),
                str(record.percentile)
            ])

        if more:
            self.response.headers["next-cursor"] = next_curs.urlsafe()

        # rewind the StringIO
        data.seek(0)
        self.response.headers['Content-Type'] = "text/csv; charset=utf-8"
        self.response.write(data.read())


class AddImplant(webapp2.RequestHandler):
    def post(self):
        post_params = json.load(self.request.body_file)

        type_id = post_params["type_id"]

        implant = ImplantObject.query(
            ImplantObject.type_id == type_id
        ).get()

        if not implant:
            implant = ImplantObject(
                type_id=type_id
            )

        implant.charisma_mod = post_params["charisma"]
        implant.intelligence_mod = post_params["intelligence"]
        implant.memory_mod = post_params["memory"]
        implant.perception_mod = post_params["perception"]
        implant.willpower_mod = post_params["willpower"]

        implant.put()

        self.response.headers['Content-Type'] = "text/plain; charset=utf-8"
        self.response.write("ok")


class AddSkill(webapp2.RequestHandler):
    def post(self):
        post_params = json.load(self.request.body_file)

        type_id = post_params["type_id"]

        skill = Skill.query(
            Skill.type_id == type_id
        ).get()

        if not skill:
            skill = Skill(
                type_id=type_id
            )

        skill.primary = post_params["primary"]
        skill.secondary = post_params["secondary"]
        skill.mp = post_params["mp"]

        skill.put()

        self.response.headers['Content-Type'] = "text/plain; charset=utf-8"
        self.response.write("ok")



app = webapp2.WSGIApplication([
    #('/setup', Setup),
    ('/price_capture', PriceCapture),
    #('/load_items/(\d+)/(\d+)', LoadItems),
    #('/load_items', LoadAllItems),
    ('/watch/(\d+)/(true|false)', Watch),
    ('/profit/(\d+)/max/(\d+)', ProductionProfitMax),
    ('/profit/(\d+)/avg/(\d+)', ProductionProfitAvg),
    ('/export/(\d+)', Export),
    ('/copy_bp', CopyBP),
    ('/accounts', Accounts),
    ('/account/(\d+)', Account),
    ('/characters/(\d+)', Characters),
    ('/skills/(\d+)/(\d+)', Skills),
    ('/skill_tree/(\d+)/(\d+)', SkillTree),
    ('/bp_jobs/(\d+)/(\d+)', BPJobs),
    ('/implant', AddImplant),
    ('/skill', AddSkill),
    ('/', MainPage),    
], debug=True)

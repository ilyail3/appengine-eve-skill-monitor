__author__ = 'ilya'

import sqlite3
from os.path import expanduser, join
import urllib
import json

# SERVER = "localhost:8080"
SERVER = "modern-dream-91412.appspot.com"

attr_ids = {
    "memory": 177,
    "willpower": 179,
    "intelligence": 176,
    "perception": 178,
    "charisma": 175
}

r_attr_ids = dict([(v, k) for k, v in attr_ids.items()])

home_dir = expanduser("~")
db = sqlite3.connect(join(home_dir, "Downloads/sqlite-latest.sqlite"))

cursor = db.cursor()

def update_implants():
    # Get all skill implants
    value_list = ",".join(map(str, attr_ids.values()))

    cursor.execute(
        "SELECT DISTINCT typeID FROM dgmTypeAttributes WHERE attributeID IN (%s) AND valueInt > 0" % (value_list,)
    )

    typeIds = set()

    while True:
        row = cursor.fetchone()

        if row is None:
            break

        typeIds.add(row[0])

    for type_id in typeIds:
        cursor.execute(
            "SELECT attributeID, valueInt FROM dgmTypeAttributes WHERE attributeID IN (%s) AND typeID = ?" % (value_list,),
            [type_id]
        )

        imp = {"type_id": type_id}

        for k in attr_ids.keys():
            imp[k] = 0

        while True:
            row = cursor.fetchone()

            if row is None:
                break

            imp[r_attr_ids[row[0]]] = row[1]

        print "Updating implant:%d" % type_id
        assert urllib.urlopen("http://%s/implant" % SERVER, json.dumps(imp)).read() == "ok"


attr_mod = {
    164: "charisma",
    165: "intelligence",
    166: "memory",
    167: "perception",
    168: "willpower"
}


def update_skills():
    cursor.execute("SELECT DISTINCT typeID FROM dgmTypeAttributes WHERE attributeID = 180")

    typeIds = set()

    while True:
        row = cursor.fetchone()

        if row is None:
            break

        typeIds.add(row[0])

    for type_id in typeIds:
        skill = {"type_id": type_id}

        cursor.execute(
            "SELECT attributeID, valueInt, valueFloat FROM dgmTypeAttributes WHERE attributeID IN (180,181,275) AND typeID = ?",
            [type_id]
        )

        while True:
            row = cursor.fetchone()

            if row is None:
                break

            val = row[1] if row[1] else int(row[2])
            attr_id = row[0]

            if attr_id == 180:
                skill["primary"] = attr_mod[val]
            elif attr_id == 181:
                skill["secondary"] = attr_mod[val]
            elif attr_id == 275:
                skill["mp"] = val

        print "Updating skill:%d" % type_id
        assert urllib.urlopen("http://%s/skill" % SERVER, json.dumps(skill)).read() == "ok"

update_implants()
update_skills()
